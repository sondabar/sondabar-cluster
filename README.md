This project holds the information for building the sondabar cluster.

# Prerequisites

You need following software to work with this repository.

- terraform
- doctl
- kubectl >= 1.14
- gnupg
- git-secret

Can be installed with:

```bash
brew install terraform doctl kubectl git-secret gnupg
```

# Secrets 

Secrets are encrypted with the hep of git-secret and gnupg. 

Before you can execute the scripts you have to decrypt the secrets. This is done with the following command. This only works if the secrets where encrypted with your public gpg key.

```bash
git secret reveal
```

If you did some changes to secrets, you have to encrypt these changes. Do it this way and commit the changed files:

```bash
git secret hide
```

Have a look at the [git secret homepage](https://git-secret.io) if you want to introduce a new secret.

# Kubernetes cluster

The configuration for the Kubernetes cluster itself is done with terraform. At the moment there is no global state store for terraform. That's why it's not possible to use in a gitlab pipeline.

For using terraform you need a terraform.tfvars file with the digital ocean token and ssh key configuration.  

```bash
cd infrastructure
terraform init
terraform apply
```

# Kubernetes 

All apps or components of the kubernetes cluster. They are split into basic infrastructure components like traefik as ingress component and apps like the blog.

You have to the current config before you can apply changes to the cluster.

```bash
doctl kubernetes cluster list -o json | jq '.[] | select(.name=="sondabar") | .id' -r | xargs doctl kubernetes cluster kubeconfig save
```

## Infrastructure

Basic infrastructure inside the kubernetes cluster.

### Traefik

Ingress for the kubernetes cluster. Routes incoming traffic into the cluster.

```bash 
kubectl apply -k kubernetes/infrastructure/ingress
```

## Apps

There are two namespaces inside the cluster. One for development and one for production installations.

### sondabar blog

Deployment for the blog. Consists of a nginx pod with static website code.

#### dev

```bash
kubectl apply -k kubernetes/apps/sondabar/overlays/dev
```

#### prod

```bash
kubectl apply -k kubernetes/apps/sondabar/overlays/prod

```