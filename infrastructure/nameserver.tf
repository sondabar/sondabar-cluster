resource "digitalocean_domain" "sondabar" {
  name = "sondabar.de"
}

# Copy the host europe settings

//# Add ipv4 base record to the domain
//resource "digitalocean_record" "base_ipv4" {
//  domain = digitalocean_domain.sondabar.name
//  type = "A"
//  name = "@"
//  value = "80.237.132.73"
//  ttl = 86400
//}
//
//# Add ipv6 base record to the domain
//resource "digitalocean_record" "base_ipv6" {
//  domain = digitalocean_domain.sondabar.name
//  type = "AAAA"
//  name = "@"
//  value = "2a01:488:42:1000:50ed:8449:ff6d:5180"
//  ttl = 86400
//}

# Add ipv4 ftp record to the domain
resource "digitalocean_record" "ftp_ipv4" {
  domain = digitalocean_domain.sondabar.name
  type = "A"
  name = "ftp"
  value = "80.237.132.73"
  ttl = 86400
}

# Add ipv6 ftp record to the domain
resource "digitalocean_record" "ftp_ipv6" {
  domain = digitalocean_domain.sondabar.name
  type = "AAAA"
  name = "ftp"
  value = "2a01:488:42:1000:50ed:8449:ff6d:5180"
  ttl = 86400
}

# Add ipv4 mail record to the domain
resource "digitalocean_record" "mail_ipv4" {
  domain = digitalocean_domain.sondabar.name
  type = "A"
  name = "mail"
  value = "80.237.132.73"
  ttl = 86400
}

# Add ipv6 mail record to the domain
resource "digitalocean_record" "mail_ipv6" {
  domain = digitalocean_domain.sondabar.name
  type = "AAAA"
  name = "mail"
  value = "2a01:488:42:1000:50ed:8449:ff6d:5180"
  ttl = 86400
}

# Add ipv4 mailout record to the domain
resource "digitalocean_record" "mailout_ipv4" {
  domain = digitalocean_domain.sondabar.name
  type = "A"
  name = "mailout"
  value = "80.237.132.73"
  ttl = 86400
}

# Add ipv6 mailout record to the domain
resource "digitalocean_record" "mailout_ipv6" {
  domain = digitalocean_domain.sondabar.name
  type = "AAAA"
  name = "mailout"
  value = "2a01:488:42:1000:50ed:8449:ff6d:5180"
  ttl = 86400
}

//# Add ipv4 www record to the domain
//resource "digitalocean_record" "www" {
//  domain = digitalocean_domain.sondabar.name
//  type = "CNAME"
//  name = "www"
//  value = "d1gn3uqow6sdz.cloudfront.net."
//  ttl = 86400
//}

# Add mail record to the domain
resource "digitalocean_record" "mx" {
  domain = digitalocean_domain.sondabar.name
  type = "MX"
  name = "mx0"
  priority = "1"
  value = "80.237.138.5"
  ttl = 86400
}



