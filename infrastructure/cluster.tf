resource "digitalocean_kubernetes_cluster" "sondabar" {
  name = "sondabar"
  region = "fra1"
  version = "1.14.1-do.2"

  node_pool {
    name = "edge-worker-pool"
    size = "s-1vcpu-2gb"
    node_count = 1
    tags = [
      "edge-worker"
    ]
  }
}

resource "digitalocean_floating_ip" "edge" {
  region = "fra1"
}

resource "digitalocean_floating_ip_assignment" "edge" {
  ip_address = digitalocean_floating_ip.edge.id
  droplet_id = data.digitalocean_droplet.worker-node.id
  depends_on = [
    "data.digitalocean_droplet.worker-node"]
}

resource "digitalocean_firewall" "sondaedge" {
  name = "traefik-80-and-443"

  tags = [
    "edge-worker"
  ]

  inbound_rule {
    protocol = "tcp"
    port_range = "80"
    source_addresses = [
      "0.0.0.0/0"]
  }

  inbound_rule {
    protocol = "tcp"
    port_range = "443"
    source_addresses = [
      "0.0.0.0/0"]
  }
}

data "digitalocean_droplet" "worker-node" {
  name = digitalocean_kubernetes_cluster.sondabar.node_pool.0.nodes.0.name
  depends_on = [
    "digitalocean_kubernetes_cluster.sondabar"]
}

resource "digitalocean_record" "cluster_ipv4" {
  domain = digitalocean_domain.sondabar.name
  type = "A"
  name = "cluster"
  value = digitalocean_floating_ip.edge.id
  ttl = 86400
}

resource "digitalocean_record" "admin_ipv4" {
  domain = digitalocean_domain.sondabar.name
  type = "A"
  name = "admin"
  value = digitalocean_floating_ip.edge.id
  ttl = 86400
}

resource "digitalocean_record" "dev_ipv4" {
  domain = digitalocean_domain.sondabar.name
  type = "A"
  name = "dev"
  value = digitalocean_floating_ip.edge.id
  ttl = 86400
}

# Add ipv4 base record to the domain
resource "digitalocean_record" "base_ipv4" {
  domain = digitalocean_domain.sondabar.name
  type = "A"
  name = "@"
  value = digitalocean_floating_ip.edge.id
  ttl = 86400
}

# Add ipv4 www record to the domain
resource "digitalocean_record" "www" {
  domain = digitalocean_domain.sondabar.name
  type = "A"
  name = "www"
  value = digitalocean_floating_ip.edge.id
  ttl = 86400
}